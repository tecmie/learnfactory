const canvas = document.getElementById('drawingsurface')
const context = canvas.getContext('2d')

// draw diagonal line
// tells the browser where to begin
// context.beginPath()
// context.moveTo(100, 150)
// context.lineTo(75, 100)
// context.moveTo(100, 150)
// context.lineTo(450, 50)
// context.lineWidth = 10

// // set line color
// context.strokeStyle = '#f00'
// context.stroke()


// draw rectangle
// context.beginPath()
// context.rect(188, 50, 200, 100)
// context.fillStyle = 'yellow'
// context.fill()
// context.lineWidth = 7
// context.strokeStyle = 'black'
// context.stroke()

// draw a custom shape
// context.beginPath()
// context.moveTo(50, 50)
// context.lineTo(70, 250)
// context.lineTo(300, 200)
// context.closePath()
// context.fill()


// // draw circle
// const centerX = canvas.width / 2
// const centerY = canvas.height / 2
// const radius = 70
// context.beginPath()
// // Math.PI is used to calculate the circumference of a circle with 
// context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false)
// context.fillStyle = 'green'
// context.fill()
// context.lineWidth = 5
// context.strokeStyle = '#030'
// context.stroke();

// draw text
// context.font = '30px Arial'
// context.fillStyle ='red'
// context.strokeText('Hello World', 10, 50)
// context.strokeText = 'blue'
// context.stroke()

// linear gradient
// context.rect(0, 0, canvas.width, canvas.height)
// // add linear gradient
// const grd = context.createLinearGradient(0, 0, canvas.width, canvas.height)
// // light blue
// grd.addColorStop(0, '#8ed6ff')
// // dark blue
// grd.addColorStop(1, '#004cb3')
// context.fillStyle = grd
// context.fill()

// radial gradient
// context.rect(0, 0, canvas.width, canvas.height)
// // add linear gradient
// const grd = context.createRadialGradient(238, 50, 10, 238, 50, 300)
// // light blue
// grd.addColorStop(0, '#8ed6ff')
// // dark blue
// grd.addColorStop(1, '#004cb3')
// context.fillStyle = grd
// context.fill()

// var imageObj = new Image()
// imageObj.onload = () => {
//     context.drawImage(imageObj, 10, 50)
// }
// imageObj.src = 'https://pbs.twimg.com/profile_images/956637993317658633/1pMpjamJ_400x400.jpg'

// Animation
const animate = (element) => {
    context.clearRect(0, 0, 500, 500)
    const xPosition = element.clientX
    const yPosition = element.clientY
    context.fillRect(xPosition, yPosition, 100, 100)
}
window.addEventListener('mousemove', animate, false)


#!/bin/bash

echo 'Enter the source directory where file is contained and the name of the file you want to copy'
read sDir
echo 'Enter the destination directory'
read dDir
if [-e "$sDir"];
then
cp $sDir $dDir#
else# echo "The file or directory does not exist"
fi